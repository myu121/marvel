//
//  ListScreenController.swift
//  Marvel
//
//  Created by Michael Yu on 6/7/21.
//

import Foundation
import UIKit

class ListScreenController: UIViewController {
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        }
    }
    lazy var viewModel: ListScreenViewModel = ListScreenViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        viewModel.fetchCharacters()
    }
}

extension ListScreenController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        300
    }
}

extension ListScreenController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfCharacters()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell") as? ListTableViewCell else {
            fatalError("could not dequeue cell")
        }
        let char = viewModel.getCharacter(at: indexPath.row)
        cell.configure(char: char)
        
        return cell
    }
}

extension ListScreenController: ListScreenViewModelDelegate {
    func reloadTable() {
        tableView.reloadData()
    }
}
