//
//  MarvelCharacter.swift
//  Marvel
//
//  Created by Michael Yu on 6/7/21.
//

import Foundation

struct Characters: Decodable {
    var data: CharacterData
}

struct CharacterData: Decodable {
    var results: [MarvelCharacter]
}

struct MarvelCharacter: Decodable {
    var id: Int
    var name: String
    var description: String
    var thumbnail: Thumbnail
    
    enum CodingKeys: String, CodingKey {
        case id, name, description, thumbnail
    }
}

struct Thumbnail: Decodable {
    var path: String
    var pathExtension: String
    
    enum CodingKeys: String, CodingKey {
        case path
        case pathExtension = "extension"
    }
}
