//
//  ListTableViewCell.swift
//  Marvel
//
//  Created by Michael Yu on 6/7/21.
//

import Foundation
import Kingfisher
import UIKit

class ListTableViewCell: UITableViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    
    func configure(char: MarvelCharacter) {
        photoImageView.kf.setImage(with: URL(string: "\(char.thumbnail.path).\(char.thumbnail.pathExtension)"))
        nameLabel.text = char.name
    }
}
