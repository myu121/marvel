//
//  ListScreenViewModel.swift
//  Marvel
//
//  Created by Michael Yu on 6/7/21.
//

import Foundation

protocol ListScreenViewModelDelegate: AnyObject {
    func reloadTable()
}

class ListScreenViewModel {
    private var dataSource: [MarvelCharacter] = []
    private let router = Router<MarvelApi>()
    weak var delegate: ListScreenViewModelDelegate?
    
    init() {
        
    }
    
    func fetchCharacters() {
        router.request(.characters) { [weak self] (results: Result<Characters, AppError>) in
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                self.dataSource = data.data.results
                self.delegate?.reloadTable()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func numberOfCharacters() -> Int {
        dataSource.count
    }
    
    func getCharacter(at index: Int) -> MarvelCharacter {
        dataSource[index]
    }
}
