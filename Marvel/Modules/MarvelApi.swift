//
//  MarvelApi.swift
//  Marvel
//
//  Created by Michael Yu on 6/7/21.
//

import Foundation

let apikey = "65cc691cbbb3b3fda1b4f2dda352443b"
let hash = "d3939d634e2782e9daff5a92b7151253"

enum MarvelApi {
    case characters
    case character(id: Int)
}

extension MarvelApi: EndPointType {
    var path: String {
        switch self {
        case .characters:
            return "v1/public/characters"
        case .character(let id):
            return "v1/public/characters/\(id)"
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .characters, .character(_):
            return .requestParameters((bodyParameters: nil,
                                      urlParameters: [
                                        "apikey": apikey,
                                        "hash": hash,
                                        "ts": 1]))
        }
    }
}
