//
//  ParameterEncoding.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import Foundation

typealias Parameters = [String: Any]

protocol ParameterEncoder {
    static func encode(urlRequest: inout URLRequest,
                       with parameters: Parameters) throws
}
