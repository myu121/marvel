# Marvel iOS App

This is an iOS application to display Marvel characters from the Marvel API.

Current functionality includes:

- List of Marvel Characters and photos
- Detail screen with description and photo


<img src="https://i.imgur.com/tW3tbO9.png">  <img src="https://i.imgur.com/Gw6awz9.png">


## Techniques

- JSON API fetching and parsing
- UIKit
- Table View
- Model-View-ViewModel (MVVM) code structure
- Delegates

## API Reference

https://developer.marvel.com/

## Technologies

- iOS 13.0 or above

- Xcode 12.5

- Swift 5

## Frameworks

- UIKit

## Supported Devices

- iPhone SE (2nd gen)

- iPhone 8 - 12 (All sizes supported)

